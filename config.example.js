export const CONFIG = {
  BITBUCKET: {
    USERNAME: 'eliecermolinauruit', // your bitbucket username
    APP_PASSWORD: '', // Bitbucket app password https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/
    COOKIE: '', // Your bitbucket cookie, take it from the browser
    AUTHORS: [
      'https://bitbucket.org/mnv_tech/psv-services/pull-requests?state=OPEN&author=%7Bc76794d7-84b4-44a9-aaf9-bb3c5f60d04d%7D',
      'https://bitbucket.org/mnv_tech/psv-services/pull-requests?state=OPEN&author=%7B3ec39de3-408c-42f6-b8dc-1d50f4c58fe5%7D',
      'https://bitbucket.org/mnv_tech/psv-services/pull-requests?state=OPEN&author=%7Bbf468ef5-9288-47d7-aa44-192652453180%7D',
      'https://bitbucket.org/mnv_tech/psv-services/pull-requests?state=OPEN&author=%7B04c13d72-cf5b-4578-b11e-db4094c47f1c%7D',
      'https://bitbucket.org/mnv_tech/psv-services/pull-requests?state=OPEN&author=%7B08759798-72c1-4463-9cb1-479bc7b524d8%7D',
      'https://bitbucket.org/mnv_tech/psv-services/pull-requests/?state=OPEN&author=%7Bb748197c-050b-4338-a697-4b7366ba048a%7D',
    ],
  },
};
