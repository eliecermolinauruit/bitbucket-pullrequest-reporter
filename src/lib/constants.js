export const CONSTANTS = {
  BUILD_STATUS: {
    SUCCESSFUL: 'Success',
    FAILED: 'Failed',
    PROGRESS: 'In Progress',
    NO_INFO: 'Nothing to show',
  },
  TASK_STATUS: {
    RESOLVED: 'RESOLVED',
    UNRESOLVED: 'UNRESOLVED',
  },
};

export const BUILD_STATUS_CHARS = {
  [CONSTANTS.BUILD_STATUS.SUCCESSFUL]: '',
  [CONSTANTS.BUILD_STATUS.FAILED]: '❌',
  [CONSTANTS.BUILD_STATUS.PROGRESS]: '⚙️',
  [CONSTANTS.BUILD_STATUS.NO_INFO]: '❔',
  default: '',
};
