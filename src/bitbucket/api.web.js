import fetch from 'node-fetch';
import { CONFIG } from '../../config';

const reqOpts = { method: 'GET', headers: { cookie: CONFIG.BITBUCKET.COOKIE } };
const requestToBitbucketApi = async (uri) => {
  const data = await fetch(uri, reqOpts);
  const json = await data.json();
  return json;
};

export const getBranchSyncInfo = async PullRequestId => await requestToBitbucketApi(`https://bitbucket.org/!api/internal/repositories/mnv_tech/psv-services/pullrequests/${PullRequestId}/branch-sync-info`);
export const getMergeRestrictions = async PullRequestId => await requestToBitbucketApi(`https://bitbucket.org/!api/internal/repositories/mnv_tech/psv-services/pullrequests/${PullRequestId}/merge-restrictions`);
export const getTasks = async PullRequestId => await requestToBitbucketApi(`https://bitbucket.org/!api/internal/repositories/mnv_tech/psv-services/pullrequests/${PullRequestId}/tasks?pagelen=1000`);
