import { CONSTANTS } from '../lib/constants';
import {
  getDiffStats,
  getUserPullRequestInfoFromURI,
  haveFilesInPrConflicts,
  getActivityData,
  getBuildStatus,
} from './api.v2';

import { getBranchSyncInfo, getTasks } from './api.web';

export const extractApprovalDataFromPullRequestActivity = (data) => {
  const { values } = data;
  const info = {
    currentApprovals: 0,
    authorComments: 0,
    otherComments: 0,
    approvalsBeforeUpdate: 0,
  };
  let wasUdpated = false;
  for (const event of values) {
    const {
      update,
      approval,
      // comment, we may do something with this info
    } = event;


    if (update) wasUdpated = true;
    if (approval) {
      if (!wasUdpated) info.currentApprovals++;
      else info.approvalsBeforeUpdate++;
    }
  }
  return info;
};

export const checkForUnResolvedTasks = ({ values }) => values.reduce(
  (thereAreUnresolvedTasks, task) => thereAreUnresolvedTasks || task.state === CONSTANTS.TASK_STATUS.UNRESOLVED
  , false
);

export const getPRReportByAuthorURI = async (URI, opts) => {
  const userPRInfo = await getUserPullRequestInfoFromURI(URI);
  if (!userPRInfo) return null;
  const { author, reports: UserPRreports } = userPRInfo;

  const {
    onUserStart = () => true,
    onRetrieve = () => true,
    onConflict = () => true,
    onActivity = () => true,
    onCommitsBehind = () => true,
    onBuildStatus = () => true,
    onTaskReceived = () => true,
  } = opts;

  onUserStart(undefined, author, URI);

  for (let idx = 0; idx < UserPRreports.length; idx++) {
    const PR = UserPRreports[idx];
    const { id } = PR;

    onRetrieve(undefined, PR);

    const { values: diffStatsData } = await getDiffStats(id);
    const conflicts = await haveFilesInPrConflicts(diffStatsData);
    onConflict(undefined, PR, conflicts);

    const activityData = await getActivityData(id);
    const { currentApprovals, approvalsBeforeUpdate } = extractApprovalDataFromPullRequestActivity(activityData);
    onActivity(undefined, PR, activityData, { currentApprovals, approvalsBeforeUpdate });

    const { behind: commitsBehind } = await getBranchSyncInfo(id);
    onCommitsBehind(undefined, PR, commitsBehind);

    const buildStatus = await getBuildStatus(id);
    onBuildStatus(undefined, PR, buildStatus);

    const tasksData = await getTasks(id);
    const haveTaskToResolve = checkForUnResolvedTasks(tasksData);
    onTaskReceived(undefined, haveTaskToResolve);

    const data = {
      ...UserPRreports[idx],
      commitsBehind: Number(commitsBehind),
      haveTaskToResolve,
      currentApprovals,
      approvalsBeforeUpdate,
      conflicts,
      buildStatus,
    };

    UserPRreports[idx] = data;
  }

  return { author, report: UserPRreports };
};

export const getBatchReports = async (authors = [], opts) => {
  const userReports = [];
  for (const authorURI of authors) {
    const authorReport = await getPRReportByAuthorURI(authorURI, opts);
    if (authorReport) userReports.push(authorReport);
  }
  return userReports;
};

