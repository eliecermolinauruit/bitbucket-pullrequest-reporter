import fetch from 'node-fetch';
import { CONFIG } from '../../config';
import btoa from '../lib/btoa';
import { CONSTANTS } from '../lib/constants';

const authRaw = btoa(`${CONFIG.BITBUCKET.USERNAME}:${CONFIG.BITBUCKET.APP_PASSWORD}`);
const reqOpts = { method: 'GET', headers: { authorization: `Basic ${authRaw}` } };
const requestToBitbucketApi = async (uri) => {
  const data = await fetch(uri, reqOpts);
  const json = await data.json();
  return json;
};

export const getUserPRList = async author => await requestToBitbucketApi(`https://api.bitbucket.org/2.0/pullrequests/${author}?${new URLSearchParams({ state: 'OPEN' })}`);
export const getActivityData = async PullRequestId => await requestToBitbucketApi(`https://api.bitbucket.org/2.0/repositories/mnv_tech/psv-services/pullrequests/${PullRequestId}/activity`);
export const getDiffStats = async PullRequestId => await requestToBitbucketApi(`https://api.bitbucket.org/2.0/repositories/mnv_tech/psv-services/pullrequests/${PullRequestId}/diffstat`);
export const getStatuses = async PullRequestId => await requestToBitbucketApi(`https://api.bitbucket.org/2.0/repositories/mnv_tech/psv-services/pullrequests/${PullRequestId}/statuses`);
export const getAuthorUUIDFromURI = url => new URLSearchParams(url).get('author');
export const haveFilesInPrConflicts = data => data.reduce((haveConflicts, { status }) => haveConflicts || status === 'merge conflict', false);

export const getBuildStatus = async (id) => {
  const { values: statuses } = await getStatuses(id);
  if (!statuses.length) return CONSTANTS.BUILD_STATUS.NO_INFO;

  const lastStatus = statuses[0];
  const { state: buildState } = lastStatus;
  let buildStatus = 'In Progress';
  if (buildState.toLocaleLowerCase() === 'failed') {
    buildStatus = CONSTANTS.BUILD_STATUS.FAILED;
  } else if (buildState.toLocaleLowerCase() === 'successful') {
    buildStatus = CONSTANTS.BUILD_STATUS.SUCCESSFUL;
  }
  return buildStatus;
};

export const getUserPullRequestInfoFromURI = async (URI) => {
  const authorUUID = getAuthorUUIDFromURI(URI);
  const UserPullRequestInfo = await getUserPRList(authorUUID);
  const { values: PRList } = UserPullRequestInfo;

  if (!PRList.length) return null;

  const { author } = PRList[0];

  const UserPRreports = PRList.map((PRInfo) => {
    const { source, destination: target, id, links, title } = PRInfo;
    const { html: { href: PrLink } } = links;
    const { branch } = source;
    const { name: branchName } = branch;
    const { branch: targetBranch } = target;
    const { name: destination } = targetBranch;

    const report = {
      id,
      title,
      branch: branchName,
      destination,
      link: PrLink,
    };
    return report;
  });

  return {
    author,
    reports: UserPRreports,
  };
};
