/* eslint-disable no-console */
import clear from 'clear';
import chalk from 'chalk';
import figlet from 'figlet';

import { getBatchReports } from '../bitbucket';
import { CONFIG } from '../../config';
import { printPullRequestReport } from './printer';

// import { ARGS } from './arguments';

export const init = async () => {
  clear();

  console.log(
    chalk.yellow(
      figlet.textSync('PR-O-Matic 3000', { horizontalLayout: 'full' })
    )
  );

  const reports = await getBatchReports(
    CONFIG.BITBUCKET.AUTHORS,
    {
      onUserStart: (error, author, URI) => (
        !error
          ? console.log(`Making report for ${author.display_name}`)
          : console.log(`❌ Unable to retrieve user report data. Uri = ${URI}`)
      ),
      onRetrieve: (error, { title }) => {
        console.log(`\t Retrieving data for ${title}`);
      },
      onConflict: error => (
        !error
          ? console.log('\t\t✅ Conflict information ready')
          : console.log('\t\t❌ Unable to retrieve conflicts information\n\t\t🔧please clone the project repo at same level of .ml folder')
      ),
      onActivity: error => (
        !error
          ? console.log('\t\t✅ Activity information ready')
          : console.log('\t\t❌ Unable to retrieve activity information')
      ),
      onCommitsBehind: error => (
        !error
          ? console.log('\t\t✅ Commit behind information ready')
          : console.log('\t\t❌ Can\'t to retrieve information about how many commits is behind against parent branch\n\t\t\t🔧 Please install the project at same level of .ml folder')
      ),
      onBuildStatus: error => (
        !error
          ? console.log('\t\t✅ Build Status information ready')
          : console.log('\t\t❌ Unable to retrieve information about build status')
      ),
      onTaskReceived: error => (
        !error
          ? console.log('\t\t✅ Taks status received')
          : console.log('\t\t❌ Unable to receive tasks')
      ),
    }
  );

  const out = await printPullRequestReport(reports);
  console.log(out);
};
