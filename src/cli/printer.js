import _orderBy from 'lodash/orderBy';
import _groupBy from 'lodash/groupBy';
import { BUILD_STATUS_CHARS, CONSTANTS } from '../lib/constants';


export const printPullRequestReport = async (reports) => {
  const REPORT_DATE_TEXT = `\nPull Request report (Generated at: ${new Date()} \n`;
  const REPORT_NOMENCLATURE = `🎉 Ready for QA
✅ Have 2 approvals
👀 Looking for approvals
☑️ Need one approval
👍 Was approved before
🍋 Easy - If title contains the word easy
✍🏻 WIP - If title contains the word WIP
⛰ Is a EPIC - If title contains the word Epic
📝 Need to attend tasks
⚠️ Have Conflicts
‼️ TOO OLD
❌ Build no passed
⚙️ Build status in progress
❔ Build status not found

If you have  not finished build you can restart those following this link: http://jenkins.mldev.cloud/blue/organizations/jenkins/branches/
`;

  let REPORT_OUTPUT = `${REPORT_DATE_TEXT}\n${REPORT_NOMENCLATURE}\n\nPull Requests:\n\n`;

  const shouldAddFlagBecauseTooOld = ({ commitsBehind }) => commitsBehind > 10;
  const buildPassed = ({ buildStatus }) => buildStatus === CONSTANTS.BUILD_STATUS.SUCCESSFUL;
  const isPRReadyForQA = ({ buildStatus, conflicts, currentApprovals, commitsBehind, haveTaskToResolve }) => (
    buildPassed({ buildStatus })
    && !shouldAddFlagBecauseTooOld({ commitsBehind })
    && !conflicts
    && currentApprovals >= 2
    && !haveTaskToResolve
  );

  const getFlags = report => [
    shouldAddFlagBecauseTooOld(report) ? '‼️' : null,
    BUILD_STATUS_CHARS[report.buildStatus],
    report.haveTaskToResolve ? '📝' : null,
    report.title.toLocaleLowerCase().indexOf('wip') > -1 ? '✍🏻' : null,
    report.title.toLocaleLowerCase().indexOf('epic') > -1 ? '⛰' : null,
    report.title.toLocaleLowerCase().indexOf('easy') > -1 ? '🍋' : null,
    report.approvalsBeforeUpdate > 0 ? '👍' : null,
  ].filter(x => !!x).join('');


  const printStatusRow = (report, icon, setFlags = true) => `${icon} ${(setFlags ? getFlags(report) : '')} ${report.link}  ${report.branch} \n`;

  const printReadyForQA = (report) => {
    REPORT_OUTPUT += printStatusRow(report, '🎉', false);
  };
  const printHaveTwoApprovals = (report) => {
    REPORT_OUTPUT += printStatusRow(report, '✅');
  };
  const printHaveConflicts = (report) => {
    REPORT_OUTPUT += printStatusRow(report, '⚠️');
  };
  const printWaitingOneApproval = (report) => {
    REPORT_OUTPUT += printStatusRow(report, '☑️');
  };
  const printWaitinLookingForApprovals = (report) => {
    REPORT_OUTPUT += printStatusRow(report, '👀');
  };

  const priorityPrinters = [
    printReadyForQA,
    printHaveConflicts,
    printHaveTwoApprovals,
    printWaitingOneApproval,
    printWaitinLookingForApprovals,
  ];

  const printReportsForUser = (authorName, authorReports) => {
    REPORT_OUTPUT += `Report For ${authorName} : \n\n`;
    authorReports.forEach(singlePrReport => priorityPrinters[singlePrReport.priority](singlePrReport));
    REPORT_OUTPUT += '\n\n';
  };

  const compactReports = [];

  for (let idxReports = 0; idxReports < reports.length; idxReports++) {
    const authorReport = reports[idxReports];
    const { author, report } = authorReport;

    for (let idxAuthorReport = 0; idxAuthorReport < report.length; idxAuthorReport++) {
      const prReport = report[idxAuthorReport];
      reports[idxReports].report[idxAuthorReport].author = author;
      if (isPRReadyForQA(prReport)) reports[idxReports].report[idxAuthorReport].priority = 0;
      else if (prReport.conflicts > 0) reports[idxReports].report[idxAuthorReport].priority = 1;
      else if (prReport.currentApprovals >= 2) reports[idxReports].report[idxAuthorReport].priority = 2;
      else if (prReport.currentApprovals === 1) reports[idxReports].report[idxAuthorReport].priority = 3;
      else if (prReport.currentApprovals === 0) reports[idxReports].report[idxAuthorReport].priority = 4;
      compactReports.push(reports[idxReports].report[idxAuthorReport]);
    }
  }
  const reportsMapByUser = _groupBy(compactReports, ({ author: { display_name: name } }) => name);
  Object
    .keys(reportsMapByUser)
    .forEach((authorName) => {
      const authorReports = reportsMapByUser[authorName];
      const orderedReportsByPriority = _orderBy(authorReports, ['priority']);
      printReportsForUser(authorName, orderedReportsByPriority);
    });

  return REPORT_OUTPUT;
};

