export const SCHEMA = {
  '--git': Boolean,
  '--yes': Boolean,
  '--install': Boolean,
  '-g': '--git',
  '-y': '--yes',
  '-i': '--install',
};
