import arg from 'arg';

import { SCHEMA } from './schema';

const args = arg(SCHEMA, { argv: process.argv.slice(2) });

export const ARGS = {
  skipPrompts: args['--yes'] || false,
  git: args['--git'] || false,
  template: args._[0],
  runInstall: args['--install'] || false,
};
