# Installation
1. `git clone`
2. Move the cloned folder repo at same level of your macmillan project
3. Inside the folder run: `npm install `

# Usage

1. ` npm start `
2. Open `report.txt`


# Configuration

1. Copy `config.example.js` and rename as `config.js`
2. Open `config.js`
3. Set your bitbucket username and your bitbucket app password (Read how to create it: https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/)
4. Add people to your report adding them to `AUTHORS` in your config file. Each author is the link when you filter PR's by author in this link: `https://bitbucket.org/mnv_tech/psv-services/pull-requests/`. If you filter by Camilo Montoya the link should be `https://bitbucket.org/mnv_tech/psv-services/pull-requests/?state=OPEN&author=%7B3ec39de3-408c-42f6-b8dc-1d50f4c58fe5%7D`
